# Machine Setup

## iTerm 2
1. https://www.iterm2.com/downloads.html

## Brew
1. `$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
2. `$ xargs brew install < brew-pkgs.txt`

## Git
1. `$ git config --global user.name "David Normington"`
2. `$ git config --global user.email "email@address.com"`
3. `$ brew install bash-completion`

## Root Files
1. `cp rootFiles/. ~/`

## Theme setup
https://github.com/chriskempson/base16-shell

## VIM (might not be needed)
1. `$ git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim`
2. `$ vim +PluginInstall +qall`
3. `$ mkdir ~/.vim/colors/`
4. `$ cp solrized.vim ~/.vim/colors/`

