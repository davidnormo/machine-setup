# Aliases
alias ll='ls -AlFh';
alias workspace='cd ~/Documents/workspace/';
alias cleanpatches='git branch | grep "arcpatch" | xargs git branch -D';
alias simulator='open /Applications/Xcode.app/Contents/Developer/Applications/iOS\ Simulator.app';
alias nextConflict='git status --short | egrep "^[A-Z?][A-Z?] " | sed "s/^...//" | head -n1';
alias cr='cat ~/CRSteps.txt';
alias rmswp='find . -name *.swp -exec rm -rf {} \;';
alias prettyJSON='underscore print --outfmt pretty';
alias t='todo.sh';
export TODOTXT_DEFAULT_ACTION=ls;
alias tmuxHelp='cat ~/tmux-cheatsheet.txt';

# Thunderbolt display camera reset
alias thunderbolt_cam='sudo killall VDCAssistant';

bower_run() {
	eval $(grep "$1" .bowerrc | sed -e 's/"//g' -e "s/.*$1: //")
}

# Exports
export CLICOLOR=1
if [ -z $TERM ]; then
  export TERM="xterm-color";
fi
export EDITOR=$(which vim)
export GIT_EDITOR=$(which vim)
export LESS='-R'
export LESSOPEN='|~/.lessfilter %s'
export JVM_ARGS="-Xms1g -Xmx4g -XX:MaxPermSize=2g"
export HISTTIMEFORMAT="%d/%m/%y %T "

# Setup theme/colours: https://github.com/chriskempson/base16-shell
BASE16_SHELL=$HOME/.config/base16-shell/
[ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"

# PATH
export PATH="/usr/local/bin:/usr/local/Cellar:~/.local/bin:~/Library/Python/2.7/bin:$PATH"

parse_git_branch() {
	local x=$(git symbolic-ref -q --short HEAD 2> /dev/null || git describe --tags --exact-match 2> /dev/null)
	if [ -n "$x" ];	then echo " ($x)"; fi
}

export PS1='\W\[\033[32m\]$(parse_git_branch)\[\033[00m\] $ '

# Git completion - https://github.com/git/git/blob/master/contrib/completion/git-completion.bash
[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion

git_shift_stash() {
	git stash -u && git checkout -b $1 && git stash pop
}


# Unit tests
tt() {
	all="all"
	grunt test:"${1-all}" --filter="$2"
}

# Vim quick edit
e() {
	vim -O $(find "www/app/modules/$1" -name "$2.js")
}

export NVM_DIR="$HOME/.nvm"
. "/usr/local/opt/nvm/nvm.sh"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

findInstances() {
	for region in `aws ec2 describe-regions --output text | cut -f3`
	do
		echo -e "\nListing Instances in region:'$region'..."
		aws ec2 describe-instances --region $region | grep -i 'ubs'
	done
}

# Arcadia specic stuff
eval "$(docker-machine env default)"
